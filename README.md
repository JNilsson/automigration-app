## Authentication and Database Migration API  
  Example of database migration and updates with client authentication.   
  This project pulls a standalone MySQL docker image & and IBM's LoopBack _(V3 LTS)_.  
  [LoopBack](https://loopback.io/doc/en/lb3/) is a highly extensible, open-source Node.js framework based on Express.  
  This allows rapid creation of APIs and micro-services composed from a MySQL backend.


### To get it up and running:  
- [Docker](https://docker.com) is a dependency, make sure it is installed  
- A [docker hub account](https://hub.docker.com/) is required  
- You must be logged-in to your docker hub account  
 `run`  
  ```$ 
  docker login
  ```  
  enter your credentials and verify the login success  

- In order to pull down the docker images  
`run`  
  ```$  
  docker-compose pull
  ```  
- To start a development environment   
`run`  
  ```$    
  docker-compose up
  ```