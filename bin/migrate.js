'use strict'

const path = require('path')

// import app for one time use
const server = require(path.resolve(__dirname, '../server/server.js'))

// ref the datasource
const mysql = server.dataSources.mysql

// loopback model tables & custom models
const base = ['User', 'AccessToken', 'ACL', 'RoleMapping', 'Role']
const custom = ['Widget', 'Client']
const lbTables = [].concat(base, custom)

// create tables
mysql.automigrate(lbTables, function (err) {
  if (err) throw err
  console.log(' ')
  console.log(' Tables[' + lbTables + '] reset in ' + mysql.adapter.name)
  console.log(' ')
  mysql.disconnect()
  process.exit(0)
}
)
